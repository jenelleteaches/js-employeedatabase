// We use an "Immediate Function" to initialize the application to avoid leaving anything behind in the global scope
(function () {

    console.log(EMPLOYEES)

    /* ---------------------------------- Local Functions ---------------------------------- */
    function addToContacts(id) {
      alert('clicked!')
    }

    function findByName() {
      alert('search button clicked');
      let searchTerm = $(".search-key").val()

      let results = EMPLOYEES.filter(function(employee){
        let fullName = employee["firstName"] + " " + employee["lastName"];
        fullName = fullName.toLowerCase()

        if (fullName.indexOf(searchTerm) > -1) {
          return true
        }
        return false
      })
      console.log(results)

      alert("adding html to page");
      var data = {
        "results":results
      }

      $(".content").html(employeeTemplate(data))

      // attache event listeners
      // add click handlers for employeeTemplates
      $(".employee-link").click(function(){
        console.log("clicked")
        let id = $(this).data("employee-id")
        alert(id)
        addToContacts(id)
      })

    }   // end findByName()


    function addToContacts(id) {
      try {
        var x = navigator.contacts.create({"displayName":"TEST USER"})
        x.save()
        alert("user saved")
      }
      catch(err) {
        alert("error adding to contact book")
      }
    }

    function showHomeView() {
      let html = ""
      html += "<h1>Employee Directory</h1>"
      html += `<input class="search-key" type="search" placeholder="Enter a name"/>`
      html += `<button id="search-btn">Search</button>`
      html += `<ul class="employee-list"></ul>`

      $("body").html(homeTemplate)
      $("#search-btn").click(findByName)


    }

    /* ----------------------------------  Main Program ---------------------------------- */

    let homeTemplate = Handlebars.compile($("#home-template").html());
    let employeeTemplate = Handlebars.compile($("#employee-list-template").html());

    showHomeView();



}());
