## How to run

Download repository

To run it in browser, open a Terminal and type:

`cordova run browser`

To run in IOS, open Termin and type:

`cordova run ios`

To run on Android, upload code to `build.phonegap.com`

Use your QR code scanner to download the APK
